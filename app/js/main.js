$(document).ready(function(){

    $('.banner-slider').slick({
        autoplay: true,
        dots: true,
        infinite: true,
        speed: 1500,
        slidesToShow: 1,
        arrows: false
    });

    $('.customers__slider').slick({
        // autoplay: true,
        dots: true,
        infinite: true,
        speed: 1500,
        slidesToShow: 3,
        arrows: false
    });

    $('.js-customers__link').click(function (e) {
        e.preventDefault();
        var imagePath = $(this).find('.customers__item-image').attr('src');
        $('.modal-fade, .customers__modal').addClass('show');
        $('.customers__modal img').attr('src', imagePath);
    });

    $('.modal-fade').click(function () {
        $('.modal-fade, .customers__modal').removeClass('show');
        $('.customers__modal img').attr('src', '');
    });

    if ($('#map').length > 0) {
        ymaps.ready(init);
        function init() {
            var myMap = new ymaps.Map("map", {
                center: [53.12276125067253, 158.51538099999993],
                zoom: 16
            });
        }
    }

});