(function () {
	'use strict';

// CANVAS LINE
	function convasObj(_x,_y){
		var posX = _x;
		var posY = _y;
		var canvas = document.querySelector( '#canvas' ),
			context = canvas.getContext('2d');

			context.clearRect(0, 0, 1064, 555);

			context.beginPath(); // новый путь
			context.strokeStyle = 'rgba(0, 224, 208, 0.7)';
			context.lineWidth = '2';
			context.moveTo(850,165);
			context.lineTo(posX, posY);
			context.stroke();
			context.closePath();

	}
	convasObj(240, 279);

	// position marker
	$(function(){
		var _marker = $('.js-marker');

		for(var i = 0; i < _marker.length; i++){
			_marker.eq(i).attr('data-json_ID', i);
		}

		_marker.on('click', function(event){
			var posY = $(this).position().top;
			var posX = $(this).position().left;
			var _modal = $('.js-map-modal');
			var _canvas = $('.js-canvas');
			var _time = 400;

			// var _line = $('.map-modal__circle_line');
			// _line.css({
			// 	'width': (1064 - posY) + 'px'
			// });

			_marker.removeClass('is-open');
			$(this).addClass('is-open');

			if( $(this).hasClass('is-open') ){
				_modal.removeClass('is-open');
				_canvas.removeClass('is-open');

				setTimeout(function(){
					_modal.addClass('is-open');
					_canvas.addClass('is-open');
				}, _time);

			}else{
				_modal.removeClass('is-open');
				_canvas.removeClass('is-open');
			}

			_modal.css({
				'transform-origin': posX+'px' + ' ' + posY+'px',
				'-webkit-transform-origin': posX+'px' + ' ' + posY+'px',
				'-ms-transform-origin': posX+'px' + ' ' + posY+'px'
			});

			convasObj(posX+16,posY+40);

			// _circle_line.css({
			// 	'width': 'calc(100% - ' + _offset_line + 'px)',
			// 	'top': (posY+36) + 'px',
			// 	'left': (posX+15) + 'px'
			// });

			ajaxModal( $(this) );
		});
	});

	// GET AJAX
	function ajaxModal(element){
		var _content_top = $('.js-modal-content-top');
		var _content_bottom = $('.js-modal-content-bottom');

		$.ajax({
			url: 'js/json/json_data.json',  // URL
			dataType : "json",            // type
			success: function (date) { 		// event
				var _json_ID = element.attr('data-json_ID');

				for (var i in date) {
					if( date[i].ID == _json_ID ){
						_content_top.text( date[_json_ID].company );
						_content_bottom.text( date[_json_ID].info );
					}
				}
			}
		});
	}

})();