// MODAL
(function(){
	'use strict';

// Open Modal
	var _modalBtn = document.querySelectorAll('.js-modalBtn'),
			_body = document.querySelector('body');
	if( _modalBtn ){
		for (var i = 0; i < _modalBtn.length; i++) {
			_modalBtn[i].addEventListener('click', function(event){
				event.preventDefault();
					var _modalId = this.getAttribute('data-modalId'),
							_modal = document.querySelector('.js-modal' + _modalId),
							_offsetWidth = document.documentElement.offsetWidth,
							_clientWidth = document.body.clientWidth,
							_rightpadding = _offsetWidth - _clientWidth;

							_body.style.paddingRight = _rightpadding + 'px';
							_body.classList.add('no-scroll-el');
							document.documentElement.classList.add('no-scroll-el');
				_modal.classList.add('active');
			});
		}
	}

	// Close modal
	var _modals = document.querySelectorAll('.js-modal');
	for (var i = 0; i < _modals.length; i++) {
		_modals[i].addEventListener('click', function(event){
			var _target = event.target || event.currentTarget;

			if( _target.classList.contains('modal--wrap') || _target.classList.contains('js-closeModal')){
				this.classList.remove('active');
				_body.removeAttribute('style');
				_body.classList.remove('no-scroll-el');
				document.documentElement.classList.remove('no-scroll-el');
			}

		});
	}

}());